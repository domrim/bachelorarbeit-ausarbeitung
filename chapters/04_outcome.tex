\chapter{Auswertung}\label{chap:auswertung}
\section{Ergebnisse}\label{sec:ergebnisse}

\subsection{Vergleich mit SSPROP}\label{subsec:compare_ssprop_result}

Für den Vergleich mit SSPROP werden die Signale aus Python und Matlab im Zeit- und Frequenzbereich jeweils in einen gemeinsamen Graphen geplottet. \cref{fig:compare_ssprop} zeigt, dass beide Implementierungen nahezu identische Ergebnisse erzielen. Die Ausführungsdauer der Matlab Kanalsimulation beträgt \SI{180}{\milli\second}, die Python-Funktion benötigt mit den gleichen Parametern und Eingangswerten lediglich \SI{3.15}{\milli\second}. Damit ist die Python-Funktion circa 60 mal schneller als die Matlab-Funktion.

\begin{figure}[htb]
	\centering%
	\input{figures/plots/ssprop_vgl.tex}
	\caption{Vergleich zur SSPROP-Implementierung}
	\label{fig:compare_ssprop}
\end{figure}


\begin{table}[htb]
    \begin{minipage}[t]{0.5\linewidth}
        \centering
        \begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=3.3] s}
            \multicolumn{3}{c}{\textbf{Signalgenerierung}}\\
            \hline
            \textrm{Parameter} & {Wert}\\
            \hline
            Modulation & {BPSK}\\
            Filter & {RC}\\
            f\_symbol & 32 & \giga\Baud\\
            n\_up & 10\\
            r\_rc & 0.33\\
            syms\_per\_filt & 4\\
            P\_in & 19 & \dBm\\
            n\_symbol & 20\\
            \hline
        \end{tabular}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.5\linewidth}
        \centering
        \begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=3.3e4] s}
            \multicolumn{3}{c}{\textbf{Kanalsimulation}}\\
            \hline
            \textrm{Parameter} & {Wert}\\
            \hline
            z\_length & 70 & \km\\
            nz & 10\\
            dz & 7 & \km\\
            alpha ($\alpha$) & 0.2 & \decibel\per\km\\
            beta2 ($\beta_2$) & -2.167e-23 & \s^2\per\kilo\m\\
            gamma ($\gamma$) & 1.3 & \per\watt\per\km\\
            \hline
        \end{tabular}
    \end{minipage}
    \caption{Simulationsparameter für \cref{subsec:compare_ssprop_result}}
    \label{tab:compare_ssprop}
\end{table}

\subsection{Visualisierung}\label{subsec:propagation_visualisation_result}

In \cref{fig:propagation_visualization} lässt sich die Entwicklung des Betragsquadrates des Signals nach jedem Schritt betrachten. Die Darstellungen sind immer bei der halben Schrittweite, da bei der reduzierten symmetrischen \ac{SSFM}, wie in \cref{subsec:ssfm_sym} beschrieben, das Signal nur in der Mitte eines Schrittes auslesbar ist. Um einen möglichst großen Einfluss der Nichtlinearität beobachten zu können, wird hier der Dämpfungskoeffizient $\alpha=0$ gewählt.

\begin{table}[htb]
	\begin{minipage}[t]{0.5\linewidth}
		\centering
		\begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=3.3] s}
			\multicolumn{3}{c}{\textbf{Signalgenerierung}}\\
			\hline
			\textrm{Parameter} & {Wert}\\
			\hline
			Modulation & {BPSK}\\
			Filter & {RC}\\
			f\_symbol & 32 & \giga\Baud\\
			n\_up & 10\\
			r\_rc & 0.33\\
			syms\_per\_filt & 4\\
			P\_in & 5 & \dBm\\
			n\_symbol & 30\\
			\hline
		\end{tabular}
	\end{minipage}
	\hfill
	\begin{minipage}[t]{0.5\linewidth}
		\centering
		\begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=3.3e4] s}
			\multicolumn{3}{c}{\textbf{Kanalsimulation}}\\
			\hline
			\textrm{Parameter} & {Wert}\\
			\hline
			z\_length & 70 & \km\\
			nz & 10\\
			dz & 7 & \km\\
			alpha ($\alpha$) & 0.0 & \decibel\per\km\\
			beta2 ($\beta_2$) & -2.167e-23 & \s^2\per\kilo\m\\
			gamma ($\gamma$) & 1.3 & \per\watt\per\km\\
			\hline
		\end{tabular}
	\end{minipage}
	\caption{Simulationsparameter für \cref{subsec:propagation_visualisation_result}}
	\label{tab:propagation_visualisation}
\end{table}

\begin{figure}[p]
	\centering
	\input{figures/plots/visualisierung.tex}
	\caption{Verlauf der Änderung eines Signals auf einem \ac{LWL}}
	\label{fig:propagation_visualization}
\end{figure}

\subsection{Schrittweite}\label{subsec:stepsize_result}

Für alle Modulationen (\ac{BPSK}, \ac{QPSK}, \ac{16QAM}) wurden die Werte $nz = 1$ bis $nz = 100$, mit dem Abstand $\Delta_{nz} = 1$ zwischen zwei Simulationen, simuliert. In den \cref{fig:steps_sweep_bpsk,fig:steps_sweep_qpsk,fig:steps_sweep_16qam} ist zu sehen wie der relative Fehler, bei allen Eingangsleistungen $P_{\text{in}}$, mit steigender Schrittanzahl exponentiell abnimmt. In \cref{fig:steps_sweep_compare} ist der Relative Fehler bei $P_{\text{in}} =$ \SIlist{0; 5}{\dBm} für alle drei Modulationen zu sehen. Die Modulation \ac{16QAM} hat aufgrund ihres höheren \textit{\ac{PAPR}} immer den größten Fehler und benötigt somit die meisten Schritte um einen geringen Fehler zu erreichen. Die \ac{QPSK}-Modulation zeigt den geringsten Fehler für alle Schrittweiten auf.

\begin{table}[htb]
	\begin{minipage}[t]{0.5\linewidth}
		\centering
		\begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=4.3] s}
			\multicolumn{3}{c}{\textbf{Signalgenerierung}}\\
			\hline
			\textrm{Parameter} & {Wert}\\
			\hline
			Modulation & \multicolumn{2}{l}{BPSK, QPSK, 16QAM}\\
			Filter & {RC}\\
			f\_symbol & 32 & \giga\Baud\\
			n\_up & 10\\
			r\_rc & 0.33\\
			syms\_per\_filt & 4\\
			P\_in & \numrange{-5}{9} & \dBm\\
			n\_symbol & 1000\\
			\hline
		\end{tabular}
	\end{minipage}
	\hfill
	\begin{minipage}[t]{0.5\linewidth}
		\centering
		\begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=3.3e4] s}
			\multicolumn{3}{c}{\textbf{Kanalsimulation}}\\
			\hline
			\textrm{Parameter} & {Wert}\\
			\hline
			z\_length & 70 & \km\\
			nz & \numrange{1}{100}\\
			dz & \numrange{70}{0.7} & \km\\
			alpha ($\alpha$) & 0.2 & \decibel\per\km\\
			beta2 ($\beta_2$) & -2.167e-23 & \s^2\per\kilo\m\\
			gamma ($\gamma$) & 1.3 & \per\watt\per\km\\
			\hline
		\end{tabular}
	\end{minipage}
	\caption{Simulationsparameter für \cref{subsec:stepsize_result}, \cref{fig:steps_sweep_bpsk,fig:steps_sweep_qpsk,fig:steps_sweep_16qam}}
	\label{tab:steps_sweep_mods}
\end{table}

\begin{table}[htb]
	\begin{minipage}[t]{0.5\linewidth}
		\centering
		\begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=4.3] s}
			\multicolumn{3}{c}{\textbf{Signalgenerierung}}\\
			\hline
			\textrm{Parameter} & {Wert}\\
			\hline
			Modulation & \multicolumn{2}{l}{BPSK, QPSK, 16-QAM}\\
			Filter & {RC}\\
			f\_symbol & 32 & \giga\Baud\\
			n\_up & 10\\
			r\_rc & 0.33\\
			syms\_per\_filt & 4\\
			P\_in & \numlist{0;5} & \dBm\\
			n\_symbol & 1000\\
			\hline
		\end{tabular}
	\end{minipage}
	\hfill
	\begin{minipage}[t]{0.5\linewidth}
		\centering
		\begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=3.3e4] s}
			\multicolumn{3}{c}{\textbf{Kanalsimulation}}\\
			\hline
			\textrm{Parameter} & {Wert}\\
			\hline
			z\_length & 70 & \km\\
			nz & \numrange{1}{100}\\
			dz & \numrange{70}{0.7} & \km\\
			alpha ($\alpha$) & 0.2 & \decibel\per\km\\
			beta2 ($\beta_2$) & -2.167e-23 & \s^2\per\kilo\m\\
			gamma ($\gamma$) & 1.3 & \per\watt\per\km\\
			\hline
		\end{tabular}
	\end{minipage}
	\caption{Simulationsparameter für \cref{subsec:stepsize_result}, \cref{fig:steps_sweep_compare}}
	\label{tab:steps_sweep_compare}
\end{table}

\begin{figure}[p]
	\centering
	\input{figures/plots/step_sweep_bpsk.tex}
	\caption{Fehler für mehrere Sendeleistungen mit \ac{BPSK}-Modulation.}
	\label{fig:steps_sweep_bpsk}
\end{figure}

\begin{figure}[p]
	\centering
	\input{figures/plots/step_sweep_qpsk.tex}
	\caption{Fehler für mehrere Sendeleistungen mit \ac{QPSK}-Modulation.}
	\label{fig:steps_sweep_qpsk}
\end{figure}

\begin{figure}[p]
	\centering
	\input{figures/plots/step_sweep_16qam.tex}
	\caption{Fehler für mehrere Sendeleistungen mit \ac{16QAM}-Modulation.}
	\label{fig:steps_sweep_16qam}
\end{figure}

\begin{figure}[p]
	\centering
	\input{figures/plots/step_sweep_compare.tex}
	\caption{Vergleich des Fehlers bei mehreren Modulationen mit $P_\text{in} = $ \SIlist{0; 5}{\dBm}.}
	\label{fig:steps_sweep_compare}
\end{figure}

\subsection{Überabtastung}\label{subsec:oversampling_result}

In \cref{fig:oversampling} ist ein Ausschnitt des Signals am Ende des Kanals bei verschiedenen Überabtastungen zu sehen. Es ist deutlich erkennbar, dass bei niedrigen oder keinem Überabtasten das Signal stark verfälscht wird. In diesen Fällen werden die Maxima des Signals abgeschnitten dargestellt oder gänzlich übersprungen. Im Bild ist zu sehen, dass vor allem lokale Minima und Maxima in kurzen Abständen bei zu niedriger Überabtastung nicht abgebildet werden können.

\begin{table}[p]
    \begin{minipage}[t]{0.5\linewidth}
        \centering
        \begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=3.3] s}
            \multicolumn{3}{c}{\textbf{Signalgenerierung}}\\
            \hline
            \textrm{Parameter} & {Wert}\\
            \hline
            Modulation & {BPSK}\\
            Filter & {RC}\\
            f\_symbol & 32 & \giga\Baud\\
            n\_up & \multicolumn{2}{l}{\numlist{1;2;3;4;10;16}}\\
            r\_rc & 0.33\\
            syms\_per\_filt & 4\\
            P\_in & 19 & \dBm\\
            n\_symbol & 30\\
            \hline
        \end{tabular}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.5\linewidth}
        \centering
        \begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=3.3e4] s}
            \multicolumn{3}{c}{\textbf{Kanalsimulation}}\\
            \hline
            \textrm{Parameter} & {Wert}\\
            \hline
            z\_length & 70 & \km\\
            nz & 10\\
            dz & 7 & \km\\
            alpha ($\alpha$) & 0.2 & \decibel\per\km\\
            beta2 ($\beta_2$) & -2.167e-23 & \s^2\per\kilo\m\\
            gamma ($\gamma$) & 1.3 & \per\watt\per\km\\
            \hline
        \end{tabular}
    \end{minipage}
    \caption{Simulationsparameter für \cref{subsec:oversampling_result}}
    \label{tab:oversampling}
\end{table}

\begin{figure}[p]
	\centering
	\input{figures/plots/oversampling.tex}
	\caption{Überabtastung (Ausschnitt)}
	\label{fig:oversampling}
\end{figure}

\subsection{Übertragung über lange Distanzen}\label{subsec:long_distance_result}

Der Verlauf der Leistung des Signals über die gesamte Strecke der hintereinander geschalteten \ac{LWL} ist in \cref{fig:long_distance_power} zu sehen. Diese Simulation wird mit $\alpha = \SI{0.2}{\decibel\per\km}$ durchgeführt. In \cref{fig:long_distance} ist der Eingang und der Ausgang der \ac{LWL}-Strecke abgebildet. Die Eingangsleistung wird bewusst hoch (\SI{19}{\dBm}) gewählt, damit die Nichtlinearen Effekte gut zu sehen sind. Um die Erkennbarkeit der Signale zu verbessern und den darzustellenden Zeitbereich möglichst klein zu halten werden nur vier Symbole übertragen.

\begin{table}[p]
    \begin{minipage}[t]{0.5\linewidth}
        \centering
        \begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=3.3] s}
            \multicolumn{3}{c}{\textbf{Signalgenerierung}}\\
            \hline
            \textrm{Parameter} & {Wert}\\
            \hline
            Modulation & {BPSK}\\
            Filter & {RC}\\
            f\_symbol & 32 & \giga\Baud\\
            n\_up & 10\\
            r\_rc & 0.33\\
            syms\_per\_filt & 4\\
            P\_in & 19 & \dBm\\
            n\_symbol & 4\\
            \hline
        \end{tabular}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.5\linewidth}
        \centering
        \begin{tabular}[t]{ >{\ttfamily\selectfont}l S[table-format=3.3e4] s}
            \multicolumn{3}{c}{\textbf{Kanalsimulation}}\\
            \hline
            \textrm{Parameter} & {Wert}\\
            \hline
            full\_length & 800 & \km\\
            z\_length & 70 & \km\\
            nz & 10\\
            dz & 7 & \km\\
            alpha ($\alpha$) & 0.2 & \decibel\per\km\\
            beta2 ($\beta_2$) & -2.167e-23 & \s^2\per\kilo\m\\
            gamma ($\gamma$) & 1.3 & \per\watt\per\km\\
            \hline
        \end{tabular}
    \end{minipage}
    \caption{Simulationsparameter für \cref{subsec:long_distance_result}}
    \label{tab:long_distance}
\end{table}

\begin{figure}[p]
	\centering
	\input{figures/plots/long_distance_power.tex}
	\caption{Signalleistung auf \SI{800}{\km} langem in \SI{70}{\km} Abschnitte unterteilter \ac{LWL}. Nach jedem Abschnitt ein Verstärker.}
	\label{fig:long_distance_power}
\end{figure}
\begin{figure}[p]
	\centering
	\input{figures/plots/long_distance.tex}
	\caption{Signal am Eingang und am Ausgang des \SI{800}{\km} \ac{LWL}}
	\label{fig:long_distance}
\end{figure}


\section{Diskussion}\label{sec:diskussion}

Mit den Ergebnissen in \cref{subsec:compare_ssprop_result} wird gezeigt, dass die im Rahmen dieser Arbeit programmierte Implementierung die selbe Funktion wie die SSPROP-Implementierung in Matlab hat. Dabei benötigt die Python Variante im Durchschnitt lediglich $1/60$ der Zeit, die SSPROP für die selbe Berechnung benötigt. Grund hierfür dürfte die effiziente NumPy-Bibliothek sein, da diese für die \ac{FFT}- und \ac{IFFT}-Aufrufe keine in Python implementierten Funktionen aufruft, sondern via \ac{API} Aufruf die Fortran-Bibliothek "`FFTPACK"' nutzt.\\

Aus \cref{subsec:stepsize_result} lässt sich folgern, dass auch die Modulationsart einen Einfluss auf die Qualität des Ausgangssignals hat. Für geringe Schrittweiten profitieren hier Modulationen die ein geringeres \textit{\ac{PAPR}} haben. Dies lässt sich damit erklären, dass starke Energieschwankungen im Signal auch stärker durch den nichtlinearen Parameter verzerrt werden. Insgesamt kann festgestellt werden, dass in der Regel die Schrittweite $nz = 10$ genügt um ein ausreichend genaues Ergebnis zu erhalten. Bei niedrigen Eingangsleistungen und Modulationen mit geringen \ac{PAPR} können sogar weniger Schritte zu einem Ergebnis mit der gleichen Genauigkeit führen.\\

\cref{subsec:oversampling_result} lässt sich mit dem Shannon-Nyquist-Abtasttheorem erklären. Hat das Signal zu wenige Abtastwerte liegen die Spektren im Frequenzbereich zu nah beieinander. Wenn sich das Spektrum des Signals, aufgrund der Nichtlinearität des \ac{LWL}, "`verbreitert"', überlappt es sich mit den periodischen Fortsetzungen.\\

\cref{subsec:long_distance_result} zeigt sehr anschaulich, wieso das Signal genügend Nullwerte am Anfang und am Ende benötigt. Das Signal ist am Anfang noch sehr "`schmal"' im Zeitbereich und am Ende nach \SI{800}{\km} beinahe über die gesamte Breite des Zeitbereichs verzerrt. Daraus lässt sich die Bedingung herleiten, dass Pakete (aus mehreren Symbolen) bei einer Langstreckenübertragung auf einem \ac{LWL} in genügend großem Abstand gesendet werden müssen. Auch wenn Symbole einzeln gesendet werden, müssen sie mit ausreichendem Abstand übertragen werden. Je nach Distanz ist für ein Symbol ein 10- bis 20-faches Zeitintervall nötig.