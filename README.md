# Ausarbeitung Bachelorarbeit

Dieses Repository beinhält alle Dateien meiner Ausarbeitung für meine Bachelorarbeit die ich von Juli 2019 bis Januar 2020 am CEL verfasst habe.

## Hinweise

Als Biblatex-Backend kommt `biber` zum Einsatz.
Für die LaTeX-Komplierung kommt `pdflatex` zum Einsatz.
